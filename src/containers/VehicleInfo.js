import * as React from 'react';
import { Card, IconButton } from 'react-native-paper';

const VehicleInfo = () => (
  <Card>
    <Card.Title 
      title="Card Title" 
      subtitle="Card Subtitle"
      right={(props) => <IconButton {...props} icon="dots-vertical" onPress={() => {}} />}/>
    <Card.Cover source={{ uri: 'https://picsum.photos/600' }} />
  </Card>
);

export default VehicleInfo;