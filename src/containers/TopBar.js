import * as React from 'react';
import { Appbar } from 'react-native-paper';
import { StyleSheet } from 'react-native';

const MORE_ICON = Platform.OS === 'ios' ? 'dots-horizontal' : 'dots-vertical';

const TopBar = () => (
 <Appbar style={styles.top}>
   <Appbar.Content title="ProjectFE"/>
   <Appbar.Action icon={MORE_ICON} onPress={() => {}} />
  </Appbar>
 );

export default TopBar;

const styles = StyleSheet.create({
  bottom: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
  },
});