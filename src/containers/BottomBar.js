import * as React from 'react';
import { BottomNavigation, Text } from 'react-native-paper';
import VehicleProfile from './VehicleProfile'

const HomeRoute = () => <Text>Home</Text>;

const LiveDataRoute = () => <Text>Live Data</Text>

const BottomBar = () => {
  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    { key: 'home', title: 'Home', icon: 'home' },
    { key: 'live_data', title: 'Live Data', icon: 'car-connected' },
    { key: 'vehicle_info', title: 'Vehicle Info', icon: 'car-settings'}
  ]);

  const renderScene = BottomNavigation.SceneMap({
    home: HomeRoute,
    live_data: LiveDataRoute,
    vehicle_info: VehicleProfile
  });

  return (
    <BottomNavigation
      navigationState={{ index, routes }}
      onIndexChange={setIndex}
      renderScene={renderScene}
    />
  );
};

export default BottomBar;