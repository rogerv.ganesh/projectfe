const globalState = {
  name: "ProjectFE"
}

export default function global(state = globalState, action) {
  switch(action.type) {
    case 'SET_NAME':
      return {
        ...state,
          name: action.text
      }

    default:
      return state;
  }
}